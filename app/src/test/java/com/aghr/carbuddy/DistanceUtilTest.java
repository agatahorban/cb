package com.aghr.carbuddy;

import android.location.Location;

import com.aghr.carbuddy.model.ItemType;
import com.aghr.carbuddy.model.LocItem;
import com.aghr.carbuddy.util.DistanceUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

/**
 * Created by macbook on 16.03.2017.
 */

@RunWith(RobolectricTestRunner.class)
@Config(manifest=Config.NONE)
public class DistanceUtilTest {
    Location yourLocation;

    @Before
    public void setUp(){
        yourLocation = Mockito.mock(Location.class);
        when(yourLocation.getLatitude()).thenReturn(52.247618);
        when(yourLocation.getLongitude()).thenReturn(21.069609);
    }


    @Test
    public void carCloseAndWalletNotClose_test(){
        //given
        LocItem wallet = new LocItem(52.247605,21.069814, ItemType.WALLET);
        LocItem car = new LocItem(52.247214,21.069666, ItemType.CAR);
        //when
        boolean showNotif = DistanceUtil.shouldShowNotification(yourLocation,wallet,car);
        //then
        assertThat(showNotif).isTrue();
    }

    @Test
    public void carCloseAndWalletClose_test(){
        //given
        LocItem wallet = new LocItem(52.247618, 21.069609, ItemType.WALLET);
        LocItem car = new LocItem(52.247214,21.069666, ItemType.CAR);
        //when
        boolean showNotif = DistanceUtil.shouldShowNotification(yourLocation,wallet,car);
        //then
        assertThat(showNotif).isFalse();
    }

    @Test
    public void carNotCloseAndWalletClose_test(){
        //given
        LocItem wallet = new LocItem(52.247618, 21.069609, ItemType.WALLET);
        LocItem car = new LocItem(52.246309,21.069489, ItemType.CAR);
        //when
        boolean showNotif = DistanceUtil.shouldShowNotification(yourLocation,wallet,car);
        //then
        assertThat(showNotif).isFalse();
    }

    @Test
    public void carNotCloseAndWalletNotClose_test(){
        //given
        LocItem wallet = new LocItem(52.246309,21.069489, ItemType.WALLET);
        LocItem car = new LocItem(52.246309,21.069489, ItemType.CAR);
        //when
        boolean showNotif = DistanceUtil.shouldShowNotification(yourLocation,wallet,car);
        //then
        assertThat(showNotif).isFalse();
    }


}
