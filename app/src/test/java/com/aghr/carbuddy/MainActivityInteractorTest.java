package com.aghr.carbuddy;

import com.aghr.carbuddy.model.ItemType;
import com.aghr.carbuddy.model.LocItem;
import com.aghr.carbuddy.repository.LocItemRepository;
import com.google.android.gms.maps.model.LatLng;

import org.junit.Test;
import org.mockito.Mockito;

import static junit.framework.Assert.assertTrue;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by macbook on 16.03.2017.
 */

public class MainActivityInteractorTest {
    LocItemRepository mockRepository = Mockito.mock(LocItemRepository.class);
    MainActivityInteractor sut = new MainActivityInteractorImpl(mockRepository);

    @Test
    public void createProperMarker_test(){
        //given
        String[] stringArray = new String[]{"Car","Wallet"};
        String selectedString = "Car";
        double lat = 51.40547;
        double lng = 19.70321;
        LatLng latLng = new LatLng(lat, lng);

        //when
        LocItem result = sut.createProperMarker(stringArray,selectedString,latLng);

        //then
        assertThat(result).isNotNull();
        assertThat(result.getItemType()).isEqualTo(ItemType.CAR);
        assertThat(result.getLatitude()).isEqualTo(lat);
        assertThat(result.getLongitude()).isEqualTo(lng);
    }

}
