package com.aghr.carbuddy.repository;

import android.content.Context;

import com.aghr.carbuddy.model.ItemType;
import com.aghr.carbuddy.model.LocItem;
import com.aghr.carbuddy.util.SharedPreferencesUtil;
import com.aghr.carbuddy.util.Variables;
import com.google.gson.Gson;

/**
 * Created by macbook on 12.03.2017.
 */

public class LocItemRepositoryShaPrefImpl implements LocItemRepository {
    private Context context;
    private Gson gson;

    public LocItemRepositoryShaPrefImpl(Context context){
        this.context = context;
        gson = new Gson();
    }

    @Override
    public LocItem getItem(ItemType type) {
        String itemString = "";
        switch(type){
            case CAR:
                itemString = SharedPreferencesUtil.getStringFromSharedPreferences(context, Variables.CURRENT_CAR_COORDS);
                break;
            case WALLET:
                itemString = SharedPreferencesUtil.getStringFromSharedPreferences(context, Variables.CURRENT_WALLET_COORDS);
                break;
        }
        if(!itemString.equals("")) {
            LocItem item = gson.fromJson(itemString, LocItem.class);
            return item;
        } else {
            return null;
        }
    }

    @Override
    public void saveItem(LocItem item) {
        String json = gson.toJson(item);
        switch(item.getItemType()){
            case CAR:
                SharedPreferencesUtil.saveStringToSharedPreferences(context, Variables.CURRENT_CAR_COORDS,json);
                break;
            case WALLET:
                SharedPreferencesUtil.saveStringToSharedPreferences(context, Variables.CURRENT_WALLET_COORDS,json);
                break;
        }
    }


}
