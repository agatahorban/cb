package com.aghr.carbuddy.repository;

import com.aghr.carbuddy.model.ItemType;
import com.aghr.carbuddy.model.LocItem;

public interface LocItemRepository {
    LocItem getItem(ItemType type);
    void saveItem(LocItem item);
}
