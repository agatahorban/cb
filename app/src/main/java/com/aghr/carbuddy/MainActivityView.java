package com.aghr.carbuddy;

import com.aghr.carbuddy.model.LocItem;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by macbook on 12.03.2017.
 */

public interface MainActivityView {
    void getMap();
    void addCarMarker(LocItem item);
    void addWalletMarker(LocItem item);
    MarkerOptions getMarkerOptions(LocItem item);
    void showConnectionDialog();
    void showGPSDialog();
    void clickStartStop();
    void checkIfStartingIsPossible();
    void startService();
    void stopService();
    void showAlerts(boolean internet, boolean gps);
    void setUpButton();
}
