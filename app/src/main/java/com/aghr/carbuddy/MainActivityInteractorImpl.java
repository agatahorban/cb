package com.aghr.carbuddy;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.aghr.carbuddy.model.ItemType;
import com.aghr.carbuddy.model.LocItem;
import com.aghr.carbuddy.repository.LocItemRepository;
import com.aghr.carbuddy.repository.LocItemRepositoryShaPrefImpl;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by macbook on 12.03.2017.
 */

public class MainActivityInteractorImpl implements MainActivityInteractor{
    private LocItemRepository locItemRepository;

    public MainActivityInteractorImpl(Context context){
        locItemRepository = new LocItemRepositoryShaPrefImpl(context);
    }

    public MainActivityInteractorImpl(LocItemRepository locItemRepository){
        this.locItemRepository = locItemRepository;
    }

    public LocItem createProperMarker(String[] itemStringArray, String spinnerSelectedItem, LatLng latLng){
        LocItem item;
        if(spinnerSelectedItem.equals(itemStringArray[0])) {
            item = new LocItem(latLng.latitude,latLng.longitude, ItemType.CAR);
        } else {
            item = new LocItem(latLng.latitude,latLng.longitude, ItemType.WALLET);
        }
        locItemRepository.saveItem(item);
        return item;
    }

    @Override
    public LocItem getCarItemIfExists() {
        return getItemIfExists(ItemType.CAR);
    }

    @Override
    public LocItem getWalletItemIfExists() {
        return getItemIfExists(ItemType.WALLET);
    }

    @Override
    public boolean hasInternetConnection(Context context) {
        int[] networkTypes = {ConnectivityManager.TYPE_MOBILE,
                ConnectivityManager.TYPE_WIFI};
        try {
            ConnectivityManager connectivityManager =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            for (int networkType : networkTypes) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null &&
                        activeNetworkInfo.getType() == networkType)
                    return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    @Override
    public boolean hasGPSConnection(LocationManager locationManager) {
       return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private LocItem getItemIfExists(ItemType type){
        return locItemRepository.getItem(type);
    }

}



