package com.aghr.carbuddy;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;

import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Spinner;
import android.widget.Toast;

import com.aghr.carbuddy.model.LocItem;
import com.aghr.carbuddy.services.LocServiceGoogleClient;
import com.aghr.carbuddy.util.Variables;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mehdi.sakout.fancybuttons.FancyButton;

public class MainActivity extends AppCompatActivity implements MainActivityView, OnMapReadyCallback {
    private MainActivityPresenter presenter;
    private GoogleMap mMap;

    private Marker carMarker, walletMarker, yourMarker;

    @BindView(R.id.item_spinner)
    Spinner spinner;
    @BindView(R.id.start_stop_FB)
    FancyButton startStopButton;

    @BindArray(R.array.item_array)
    String[] itemStringArray;
    @BindString(R.string.your_car)
    String yourCarString;
    @BindString(R.string.your_wallet)
    String yourWalletString;
    @BindString(R.string.gps_info)
    String gpsInfo;
    @BindString(R.string.internet_info)
    String internetInfo;
    @BindString(R.string.go_to_settings_page)
    String goToSettingsPage;
    @BindString(R.string.cancel)
    String cancel;
    @BindString(R.string.ok)
    String ok;
    @BindString(R.string.start_tracking)
    String startTracking;
    @BindString(R.string.stop_tracking)
    String stopTracking;
    @BindString(R.string.service_started)
    String serviceStarted;
    @BindString(R.string.service_stopped)
    String serviceStopped;

    private BroadcastReceiver broadcastReceiver;

    public static String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET};

    public static int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new MainActivityPresenterImpl(this, getApplicationContext());
        presenter.saveIsStarted(this,false);
        ButterKnife.bind(this);

        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !hasPermissions(this,PERMISSIONS)){
            requestPermissions(PERMISSIONS,REQUEST_CODE);
        } else {
            getMap();
        }
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        setUpButton();

        if(broadcastReceiver == null){
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    double lat = intent.getDoubleExtra(Variables.Y_LAT,-1);
                    double lng = intent.getDoubleExtra(Variables.Y_LNG,-1);
                    if(mMap!=null && lat!=-1 && lng!=-1){
                        addYouOnMap(lat,lng);
                    }
                }
            };
            registerReceiver(broadcastReceiver,new IntentFilter(Variables.LOC_SERVICE_GC));
        }
    }

    @Override
    public void getMap() {
        if (mMap == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    public void initMap(GoogleMap map) {
        mMap = map;
        if (mMap != null) {
            presenter.addMarkersIfExist();
            mMap.setOnMapLongClickListener(latLng -> presenter.addProperMarker(itemStringArray, spinner.getSelectedItem().toString(), latLng));
        }
    }

    public MarkerOptions getMarkerOptions(double lat, double lng) {
        MarkerOptions options = new MarkerOptions()
                .position(new LatLng(lat, lng))
                .draggable(false);
        return options;
    }

    public MarkerOptions getMarkerOptions(LocItem item) {
        MarkerOptions options = getMarkerOptions(item.getLatitude(), item.getLongitude());
        options.icon(BitmapDescriptorFactory.fromResource(item.getItemType().getIcon()));
        switch (item.getItemType()) {
            case CAR:
                options.snippet(yourCarString);
                options.title(yourCarString);
                break;
            case WALLET:
                options.snippet(yourWalletString);
                options.title(yourWalletString);
                break;
            default:
                break;
        }
        return options;
    }

    @Override
    public void showConnectionDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(internetInfo)
                .setCancelable(false)
                .setPositiveButton(ok, (dialog,id) -> {});
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    public void showGPSDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(gpsInfo)
                .setCancelable(false)
                .setPositiveButton(goToSettingsPage,
                        (dialog,id) -> {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            });

        alertDialogBuilder.setNegativeButton(cancel,
                (dialog,id) -> {
                        dialog.cancel();
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    @Override
    @OnClick(R.id.start_stop_FB)
    public void clickStartStop() {
        presenter.startOrStopService(this);
    }

    @Override
    public void checkIfStartingIsPossible() {
        presenter.checkGPSandInternet((LocationManager) getSystemService(Context.LOCATION_SERVICE),this);
    }

    @Override
    public void startService() {
        Intent i = new Intent(getApplicationContext(), LocServiceGoogleClient.class);
        startService(i);
        startStopButton.setText(stopTracking);
        presenter.saveIsStarted(this,true);
        Toast.makeText(this,serviceStarted,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void stopService() {
        Intent i = new Intent(getApplicationContext(),LocServiceGoogleClient.class);
        stopService(i);
        startStopButton.setText(startTracking);
        presenter.saveIsStarted(this,false);
        Toast.makeText(this,serviceStopped,Toast.LENGTH_SHORT).show();
        mMap.animateCamera(CameraUpdateFactory.zoomTo(1), Variables.INTERVAL, null);
    }

    @Override
    public void showAlerts(boolean internet, boolean gps) {
        if(!gps){
            showGPSDialog();
        }
        if(!internet){
            showConnectionDialog();
        }
    }

    @Override
    public void setUpButton() {
        if(presenter.isStarted(this)){
            startStopButton.setText(stopTracking);
        } else {
            startStopButton.setText(startTracking);
        }
    }

    @Override
    public void addCarMarker(LocItem item) {
        if (carMarker != null)
            carMarker.remove();
        carMarker = mMap.addMarker(getMarkerOptions(item));
    }

    @Override
    public void addWalletMarker(LocItem item) {
        if (walletMarker != null)
            walletMarker.remove();
        walletMarker = mMap.addMarker(getMarkerOptions(item));
    }

    public void addYouOnMap(double lat, double lng) {
        if (yourMarker != null)
            yourMarker.remove();

        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat,lng)));
//        if(yourMarker==null){
            mMap.animateCamera(CameraUpdateFactory.zoomIn());
            mMap.animateCamera(CameraUpdateFactory.zoomTo(Variables.DEFAULT_MAP_ZOOM), Variables.INTERVAL, null);
//        }
        yourMarker = mMap.addMarker(getMarkerOptions(lat, lng));
    }

    @Override
    public void onMapReady(GoogleMap map) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            initMap(map);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] perms, @NonNull int[] results) {
        if (requestCode == REQUEST_CODE) {
            if (results.length > 0 && results[0] == PackageManager.PERMISSION_GRANTED) {
                getMap();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(broadcastReceiver!=null){
            unregisterReceiver(broadcastReceiver);
        }
        stopService();
        presenter.saveIsStarted(this,false);
    }




}
