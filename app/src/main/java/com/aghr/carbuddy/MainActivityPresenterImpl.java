package com.aghr.carbuddy;

import android.content.Context;
import android.location.LocationManager;

import com.aghr.carbuddy.model.LocItem;
import com.aghr.carbuddy.util.SharedPreferencesUtil;
import com.aghr.carbuddy.util.Variables;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by macbook on 12.03.2017.
 */

public class MainActivityPresenterImpl implements MainActivityPresenter {
    private MainActivityView mainActivityView;
    private MainActivityInteractor mainActivityInteractor;

    public MainActivityPresenterImpl(MainActivityView mainActivityView, Context context) {
        this.mainActivityView = mainActivityView;
        this.mainActivityInteractor = new MainActivityInteractorImpl(context);
    }

    public void addProperMarker(String[] itemStringArray, String spinnerSelectedItem, LatLng latLng){
        LocItem item = mainActivityInteractor.createProperMarker(itemStringArray,spinnerSelectedItem,latLng);
        switch(item.getItemType()){
            case CAR:
                mainActivityView.addCarMarker(item);
                break;
            case WALLET:
                mainActivityView.addWalletMarker(item);
                break;
        }
    }

    @Override
    public void addMarkersIfExist() {
        LocItem carItem = mainActivityInteractor.getCarItemIfExists();
        LocItem walletItem = mainActivityInteractor.getWalletItemIfExists();
        if(carItem!=null){
            mainActivityView.addCarMarker(carItem);
        }
        if(walletItem!=null){
            mainActivityView.addWalletMarker(walletItem);
        }
    }

    @Override
    public boolean checkInternetConnection(Context context) {
        boolean isConnected = mainActivityInteractor.hasInternetConnection(context);
        return isConnected;
    }

    @Override
    public boolean checkGPSConnection(LocationManager locationManager) {
        boolean isConnected = mainActivityInteractor.hasGPSConnection(locationManager);
        return isConnected;
    }

    @Override
    public void checkGPSandInternet(LocationManager locationManager, Context context) {
        boolean internet = checkInternetConnection(context);
        boolean gps = checkGPSConnection(locationManager);
        boolean areGPSAndInternetOn = internet && gps;
        if(areGPSAndInternetOn) {
            mainActivityView.startService();
        } else {
            mainActivityView.showAlerts(internet,gps);
        }
    }

    @Override
    public void startOrStopService(Context context) {
        boolean isStarted = SharedPreferencesUtil.getBooleanFromSharedPreferences(context, Variables.IS_SERVICE_STARTED);
        if(isStarted){
            mainActivityView.stopService();
        } else {
            mainActivityView.checkIfStartingIsPossible();
        }
    }

    @Override
    public void saveIsStarted(Context context,boolean isStarted) {
        SharedPreferencesUtil.saveBooleanToSharedPreferences(context,Variables.IS_SERVICE_STARTED,isStarted);
    }

    @Override
    public boolean isStarted(Context context) {
        return SharedPreferencesUtil.getBooleanFromSharedPreferences(context,Variables.IS_SERVICE_STARTED);
    }
}
