package com.aghr.carbuddy.model;

import com.aghr.carbuddy.R;

public enum ItemType {
    CAR(R.drawable.ic_car),
    WALLET(R.drawable.ic_wallet);

    private final int icon;

    private ItemType(final int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }
}
