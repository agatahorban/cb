package com.aghr.carbuddy.model;

public class LocItem {
    private double latitude, longitude;
    private ItemType itemType;

    public LocItem() {
    }

    public LocItem(double latitude, double longitude, ItemType itemType) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.itemType = itemType;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }
}
