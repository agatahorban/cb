package com.aghr.carbuddy;

import android.content.Context;
import android.location.LocationManager;

import com.aghr.carbuddy.model.LocItem;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by macbook on 12.03.2017.
 */

public interface MainActivityPresenter {
    void addProperMarker(String[] itemStringArray, String spinnerSelectedItem, LatLng latLng);
    void addMarkersIfExist();
    boolean checkInternetConnection(Context context);
    boolean checkGPSConnection(LocationManager locationManager);
    void checkGPSandInternet(LocationManager locationManager, Context context);
    void startOrStopService(Context context);
    void saveIsStarted(Context context, boolean isStarted);
    boolean isStarted(Context context);
}
