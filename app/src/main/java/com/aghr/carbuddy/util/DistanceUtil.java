package com.aghr.carbuddy.util;

import android.location.Location;

import com.aghr.carbuddy.model.LocItem;

/**
 * Created by macbook on 16.03.2017.
 */

public class DistanceUtil {
    public static float countDistance(Location yourLocation, LocItem item){
        float[] itemDistances = new float[3];
        Location.distanceBetween(yourLocation.getLatitude(), yourLocation.getLongitude(), item.getLatitude(), item.getLongitude(), itemDistances);
        return itemDistances[0];
    }

    public static boolean shouldShowNotification(Location yourLocation, LocItem wallet, LocItem car){
        boolean carDistanceCorrect = countDistance(yourLocation,car) <= Variables.MINIMAL_DISTANCE_FROM_CAR;
        boolean walletDistanceCorrect = countDistance(yourLocation,wallet) >= Variables.MAX_DISTANCE_FROM_WALLET;
        return carDistanceCorrect && walletDistanceCorrect;
    }
}
