package com.aghr.carbuddy.util;

/**
 * Created by macbook on 12.03.2017.
 */

public class Variables {
    public static final String CURRENT_WALLET_COORDS = "C_C_WALLET";
    public static final String CURRENT_CAR_COORDS = "C_C_CAR";
    public static final int DEFAULT_MAP_ZOOM = 15;
    public static final int INTERVAL = 2000;
    public static final int FASTEST_INTERVAL = 1000;
    public static final float MINIMAL_DISTANCE_FROM_CAR = 50.0f;
    public static final float MAX_DISTANCE_FROM_WALLET = 10.0f;

    public static final String NOTIFICATION_CONTENT_TITLE = "You don't have your wallet with you!";
    public static final String NOTIFICATION_CONTENT_TEXT = "You should come back.";

    public static final String LOC_SERVICE = "com.aghr.carbuddy.loc_service";
    public static final String LOC_SERVICE_GC = "com.aghr.carbuddy.loc_service_gc";
    public static final String Y_LAT = "Y_LAT";
    public static final String Y_LNG = "Y_LNG";

    public static final String WAS_NEAR_CAR = "WAS_NEAR_CAR";
    public static final String IS_SERVICE_STARTED = "IS_SERVICE_STARTED";
}
