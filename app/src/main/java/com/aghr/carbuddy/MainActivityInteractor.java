package com.aghr.carbuddy;

import android.content.Context;
import android.location.LocationManager;

import com.aghr.carbuddy.model.LocItem;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by macbook on 12.03.2017.
 */

public interface MainActivityInteractor {
    LocItem createProperMarker(String[] itemStringArray, String spinnerSelectedItem, LatLng latLng);
    LocItem getCarItemIfExists();
    LocItem getWalletItemIfExists();
    boolean hasInternetConnection(Context context);
    boolean hasGPSConnection(LocationManager locationManager);
}
