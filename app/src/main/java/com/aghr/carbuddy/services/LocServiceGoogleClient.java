package com.aghr.carbuddy.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;

import com.aghr.carbuddy.R;
import com.aghr.carbuddy.model.ItemType;
import com.aghr.carbuddy.model.LocItem;
import com.aghr.carbuddy.repository.LocItemRepository;
import com.aghr.carbuddy.repository.LocItemRepositoryShaPrefImpl;
import com.aghr.carbuddy.util.DistanceUtil;
import com.aghr.carbuddy.util.SharedPreferencesUtil;
import com.aghr.carbuddy.util.Variables;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

/**
 * Created by macbook on 14.03.2017.
 */

public class LocServiceGoogleClient extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private LocationListener mLocationListener;
    private GoogleApiClient mLocationClient;
    private LocItemRepository locItemRepository;
    private boolean userLatelyNearTheCar = false;

    public void initLocationClient() {
        mLocationClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        showCurrentLocation();
        mLocationListener = location -> {
            Intent i = new Intent(Variables.LOC_SERVICE_GC);
            i.putExtra(Variables.Y_LAT, location.getLatitude());
            i.putExtra(Variables.Y_LNG, location.getLongitude());
            sendBroadcast(i);

            userLatelyNearTheCar = SharedPreferencesUtil.getBooleanFromSharedPreferences(this,Variables.WAS_NEAR_CAR);

            LocItem car = locItemRepository.getItem(ItemType.CAR);
            LocItem wallet = locItemRepository.getItem(ItemType.WALLET);
            if (car != null && wallet != null) {
                if (DistanceUtil.shouldShowNotification(location,wallet,car)) {
                    if (!userLatelyNearTheCar) {
                        showNotification();
                    }
                    userLatelyNearTheCar = true;
                } else {
                    userLatelyNearTheCar = false;
                }
                SharedPreferencesUtil.saveBooleanToSharedPreferences(this,Variables.WAS_NEAR_CAR,userLatelyNearTheCar);
            }
        };

        LocationRequest request = createRequest();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, request, mLocationListener);
    }

    @Override
    public void onConnectionSuspended(int i) {
        //TODO WARNING
    }

    private LocationRequest createRequest() {
        LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(Variables.INTERVAL);
        request.setFastestInterval(Variables.FASTEST_INTERVAL);
        return request;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        locItemRepository = new LocItemRepositoryShaPrefImpl(this);
        initLocationClient();
        mLocationClient.connect();
    }

    public void showCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.getLastLocation(mLocationClient);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //TODO WARNING
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocationServices.FusedLocationApi.removeLocationUpdates(mLocationClient, mLocationListener);
        mLocationClient.disconnect();
    }


     public void showNotification() {
         Notification n = new Notification.Builder(this)
                 .setContentTitle(Variables.NOTIFICATION_CONTENT_TITLE)
                 .setContentText(Variables.NOTIFICATION_CONTENT_TEXT)
                 .setSmallIcon(R.mipmap.ic_launcher)
                 .setAutoCancel(true)
                 .build();

         n.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;

         NotificationManager notificationManager =
                 (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

         notificationManager.notify(0, n);
     }
}
